package me.scholtes.upgradespawner.listeners;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.bgsoftware.wildstacker.api.events.SpawnerSpawnEvent;

import me.scholtes.upgradespawner.UpgradeSpawner;
import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;

public class SpawnEvent implements Listener {
	
	DataHandler dataHandler;
	UpgradeSpawner plugin;
	
	public SpawnEvent(DataHandler dataHandler, UpgradeSpawner plugin) { 
		this.dataHandler = dataHandler;
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onSpawn(SpawnerSpawnEvent e) {
		Location loc = e.getSpawner().getLocation();
		Spawner spawner = new Spawner(loc, 1, 1, 1, UUID.randomUUID().toString());
		if (dataHandler.getSpawners() != null && !dataHandler.getSpawners().isEmpty()) {
			for (Spawner tempSpawner : dataHandler.getSpawners()) {
				
				if (spawner.getLocation().getBlockX() == loc.getBlockX() && spawner.getLocation().getBlockY() == loc.getBlockY() && spawner.getLocation().getBlockZ() == loc.getBlockZ()) {
					spawner = tempSpawner;
					break;
				}
				
			}
		}
		
		int multiplier = spawner.getMultiplier();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				e.getEntity().setStackAmount(e.getEntity().getStackAmount() * multiplier, true);
			}
		}.runTaskLater(plugin, 2L);
	}

}
