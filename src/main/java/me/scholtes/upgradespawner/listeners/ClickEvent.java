package me.scholtes.upgradespawner.listeners;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;
import me.scholtes.upgradespawner.utils.Utils;
import me.swanis.mobcoins.MobCoinsAPI;

public class ClickEvent implements Listener {
	
	private Utils utils;
	private DataHandler dataHandler;
	
	public ClickEvent(Utils utils, DataHandler dataHandler) {
		this.utils = utils;
		this.dataHandler = dataHandler;
	}
	
	@EventHandler
	public void invClick(InventoryClickEvent e) {
		if (!e.getInventory().getName().equals(utils.color("&e&lUPGRADE SPAWNER"))) return;
		
		e.setCancelled(true);
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) return;
		
		if (dataHandler.getUpgrading() == null || dataHandler.getUpgrading().isEmpty() || !dataHandler.getUpgrading().containsKey(p.getUniqueId())) return;
		
		String itemName = e.getCurrentItem().getItemMeta().getDisplayName();
		List<String> lore = e.getCurrentItem().getItemMeta().getLore();
		Spawner spawner = dataHandler.getUpgrading().get(p.getUniqueId());
		int coins = MobCoinsAPI.getProfileManager().getProfile(p).getMobCoins();
		if (itemName == null || lore == null) return;
		if (itemName.equals(utils.color("&e&lSPAWNER LIMIT")) && coins >= spawner.getLimitPrice(false) && !lore.contains(utils.color("&f &f &f &f &cMaximum Limit&7"))) {
			p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
			dataHandler.getUpgrading().remove(p.getUniqueId());
			MobCoinsAPI.getProfileManager().getProfile(p).setMobCoins(coins - spawner.getLimitPrice(false));
			spawner.setLimit(spawner.getLimit() + 1);
			p.closeInventory();
			if (dataHandler.getSpawners() == null || dataHandler.getSpawners().isEmpty() || !dataHandler.getSpawners().contains(spawner)) {
				dataHandler.getSpawners().add(spawner);
				return;
			}
			dataHandler.getSpawners().remove(spawner);
			dataHandler.getSpawners().add(spawner);
		}
		if (itemName.equals(utils.color("&e&lSPAWN RATE")) && coins >= spawner.getRatePrice(false) && !lore.contains(utils.color("&f &f &f &f &cMaximum Spawn Rate&7"))) {
			p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
			dataHandler.getUpgrading().remove(p.getUniqueId());
			MobCoinsAPI.getProfileManager().getProfile(p).setMobCoins(coins - spawner.getRatePrice(false));
			spawner.setRate(spawner.getRate() + 1);
			CreatureSpawner creatureSpawner = (CreatureSpawner) spawner.getLocation().getBlock().getState();
			creatureSpawner.setMaxSpawnDelay((int) (800 * ((100 - (double) spawner.getSpawnerRate(false)) / 100)));
			creatureSpawner.setMinSpawnDelay((int) (200 * ((100 - (double) spawner.getSpawnerRate(false)) / 100)));
			spawner.getLocation().getBlock().getState().update();
			p.closeInventory();
			if (dataHandler.getSpawners() == null || dataHandler.getSpawners().isEmpty() || !dataHandler.getSpawners().contains(spawner)) {
				dataHandler.getSpawners().add(spawner);
				return;
			}
			dataHandler.getSpawners().remove(spawner);
			dataHandler.getSpawners().add(spawner);
		}
		if (itemName.equals(utils.color("&e&lSPAWN MULTIPLIER")) && coins >= spawner.getMultiplierPrice(false) && !lore.contains(utils.color("&f &f &f &f &cMaximum Spawn Multiplier&7"))) {
			p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
			dataHandler.getUpgrading().remove(p.getUniqueId());
			MobCoinsAPI.getProfileManager().getProfile(p).setMobCoins(coins - spawner.getMultiplierPrice(false));
			spawner.setMultiplier(spawner.getMultiplier() + 1);
			p.closeInventory();
			if (dataHandler.getSpawners() == null || dataHandler.getSpawners().isEmpty() || !dataHandler.getSpawners().contains(spawner)) {
				dataHandler.getSpawners().add(spawner);
				return;
			}
			dataHandler.getSpawners().remove(spawner);
			dataHandler.getSpawners().add(spawner);
		}
	}

}
