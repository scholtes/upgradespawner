package me.scholtes.upgradespawner.listeners;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;
import me.swanis.mobcoins.MobCoinsAPI;

public class BlockBreak implements Listener {

	DataHandler dataHandler;
	
	public BlockBreak(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}
	
	@EventHandler
	public void onSpawnerBreak(BlockBreakEvent e) {
		
		if (e.getBlock().getType() != Material.MOB_SPAWNER) return;
		
		Spawner spawner = new Spawner(e.getBlock().getLocation(), 1, 1, 1, UUID.randomUUID().toString());
		if (dataHandler.getSpawners() != null && !dataHandler.getSpawners().isEmpty()) {
			for (Spawner tempSpawner : dataHandler.getSpawners()) {
				
				if (spawner.getLocation().getBlockX() == tempSpawner.getLocation().getBlockX() && spawner.getLocation().getBlockY() == tempSpawner.getLocation().getBlockY() && spawner.getLocation().getBlockZ() == tempSpawner.getLocation().getBlockZ()) {
					dataHandler.getSpawners().remove(tempSpawner);
					int totalCoins = (tempSpawner.getMultiplierPrice(true) + tempSpawner.getLimitPrice(true) + tempSpawner.getRatePrice(true));
					MobCoinsAPI.getProfileManager().getProfile(e.getPlayer()).setMobCoins(MobCoinsAPI.getProfileManager().getProfile(e.getPlayer()).getMobCoins() + totalCoins);
					break;
				}
				
			}
		}
		
	}
	
}
