package me.scholtes.upgradespawner.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import me.scholtes.upgradespawner.utils.Utils;
import me.swanis.mobcoins.MobCoinsAPI;
import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;

public class SpawnerInteract implements Listener {
	
	private DataHandler dataHandler;
	private Utils utils;
	
	public SpawnerInteract(DataHandler dataHandler, Utils utils) {
		this.dataHandler = dataHandler;
		this.utils = utils;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		
		if (e.getClickedBlock() == null || e.getClickedBlock().getType() == Material.AIR || e.getClickedBlock().getType() != Material.MOB_SPAWNER) return;
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) return;
		if (e.getItem() != null && e.getItem().getType() == Material.MOB_SPAWNER) return;
		
		Spawner spawner = new Spawner(e.getClickedBlock().getLocation(), 1, 1, 1, UUID.randomUUID().toString());
		
		if (dataHandler.getSpawners() != null && !dataHandler.getSpawners().isEmpty()) {
			for (Spawner tempSpawner : dataHandler.getSpawners()) {
				
				if (spawner.getLocation().getBlockX() == tempSpawner.getLocation().getBlockX() && spawner.getLocation().getBlockY() == tempSpawner.getLocation().getBlockY() && spawner.getLocation().getBlockZ() == tempSpawner.getLocation().getBlockZ()) {
					spawner = tempSpawner;
					break;
				}
				
			}
		}
		Inventory inv = Bukkit.createInventory(null, 45, utils.color("&e&lUPGRADE SPAWNER"));
		
		List<String> lore = new ArrayList<String>();
		for (int i = 0; i < 45; i++) {
			inv.setItem(i, utils.guiItem("", lore, Material.STAINED_GLASS_PANE, (byte) 7));
		}
		lore.addAll(utils.color(Arrays.asList(
                ""
              , "&f&l \u25B6 &3&lMob Coins"
              , "&f &f &f &f &f" + MobCoinsAPI.getProfileManager().getProfile(e.getPlayer()).getMobCoins()
              , ""
              , "&f&l \u25B6 &3&lSpawner Limit"
              , "&f &f &f &f &f" + spawner.getSpawnerLimit(false)
              , ""
              , "&f&l \u25B6 &3&lSpawn Rate"
              , "&f &f &f &f &f+" + spawner.getSpawnerRate(false) + "%"
              , ""
              , "&f&l \u25B6 &3&lSpawn Multiplier &7"
              , "&f &f &f &f &f" + spawner.getMultiplier() + "x"
              , "")));
		inv.setItem(13, utils.guiItem(utils.color("&e&lINFORMATION"), lore, Material.KNOWLEDGE_BOOK, 1));
		lore.clear();
		
		String upgrade = "&f &f &f &f &f" + spawner.getSpawnerLimit(false) + " &7\u00BB &f" + spawner.getSpawnerLimit(true) + " &7";
		String cost = "&f &f &f &f &f" + spawner.getLimitPrice(false) + " mob coins";
		String canBuy = "";
		int coins = MobCoinsAPI.getProfileManager().getProfile(e.getPlayer()).getMobCoins();
		if (spawner.getLimit() == 5) {
			upgrade = "&f &f &f &f &cMaximum Limit&7";
			cost = upgrade;
			canBuy = "&f";
		} else if (coins >= spawner.getLimitPrice(false)) {
			canBuy = "&7&l &8(&aClick to upgrade&8)";
		} else {
			canBuy = "&7&l &8(&cYou can't afford this!&8)";
		}
		lore.addAll(utils.color(Arrays.asList(
                ""
              , "&f&l \u25B6 &3&lUpgrade Spawner Limit &7"
              , upgrade
              , ""
              , "&f&l \u25B6 &3&lCost"
              , cost
              , ""
              , canBuy
              , "")));
		inv.setItem(28, utils.guiItem(utils.color("&e&lSPAWNER LIMIT"), lore, Material.MOB_SPAWNER, spawner.getLimit()));
		lore.clear();
		
		upgrade = "&f &f &f &f &f+" + spawner.getSpawnerRate(false) + "% &7\u00BB &f+" + spawner.getSpawnerRate(true) + "% &7";
		cost = "&f &f &f &f &f" + spawner.getRatePrice(false) + " mob coins";
		if (spawner.getRate() == 4) {
			upgrade = "&f &f &f &f &cMaximum Spawn Rate&7";
			cost = upgrade;
			canBuy = "&f";
		} else if (coins >= spawner.getRatePrice(false)) {
			canBuy = "&7&l &8(&aClick to upgrade&8)";
		} else {
			canBuy = "&7&l &8(&cYou can't afford this!&8)";
		}
		lore.addAll(utils.color(Arrays.asList(
                ""
              , "&f&l \u25B6 &3&lUpgrade Spawn Rate &7"
              , upgrade
              , ""
              , "&f&l \u25B6 &3&lCost"
              , cost
              , ""
              , canBuy
              , "")));
		inv.setItem(31, utils.guiItem(utils.color("&e&lSPAWN RATE"), lore, Material.WATCH, spawner.getRate()));
		lore.clear();
		
		upgrade = "&f &f &f &f &f" + spawner.getMultiplier() + "x &7\u00BB &f" + (spawner.getMultiplier() + 1) + "x &7";
		cost = "&f &f &f &f &f" + spawner.getMultiplierPrice(false) + " mob coins";
		if (spawner.getMultiplier() == 4) {
			upgrade = "&f &f &f &f &cMaximum Spawn Multiplier&7";
			cost = upgrade;
			canBuy = "&f";
		} else if (coins >= spawner.getMultiplierPrice(false)) {
			canBuy = "&7&l &8(&aClick to upgrade&8)";
		} else {
			canBuy = "&7&l &8(&cYou can't afford this!&8)";
		}
		lore.addAll(utils.color(Arrays.asList(
                ""
              , "&f&l \u25B6 &3&lUpgrade Spawn Multiplier &7"
              , upgrade
              , ""
              , "&f&l \u25B6 &3&lCost"
              , cost
              , ""
              , canBuy
              , "")));
		inv.setItem(34, utils.guiItem(utils.color("&e&lSPAWN MULTIPLIER"), lore, Material.MONSTER_EGG, spawner.getMultiplier()));
		lore.clear();
		e.getPlayer().openInventory(inv);
		dataHandler.getUpgrading().put(e.getPlayer().getUniqueId(), spawner);
	}

}
