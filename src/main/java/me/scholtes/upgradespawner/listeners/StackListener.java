package me.scholtes.upgradespawner.listeners;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.bgsoftware.wildstacker.api.WildStackerAPI;
import com.bgsoftware.wildstacker.api.events.SpawnerPlaceEvent;
import com.bgsoftware.wildstacker.api.events.SpawnerStackEvent;
import com.bgsoftware.wildstacker.api.objects.StackedSpawner;

import me.scholtes.upgradespawner.nbt.NBT;
import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;

public class StackListener implements Listener {
	
	private DataHandler dataHandler;
	
	public StackListener(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}
	
	@EventHandler
	public void onStack(SpawnerStackEvent e) {
		
		StackedSpawner original = e.getSpawner();
		StackedSpawner target = e.getTarget();
		Spawner originalSpawner = new Spawner(target.getLocation(), 1, 1, 1, UUID.randomUUID().toString());
		if (dataHandler.getSpawners() != null && !dataHandler.getSpawners().isEmpty()) {
			for (Spawner spawner : dataHandler.getSpawners()) {
				
				if (spawner.getLocation().getBlockX() == original.getLocation().getBlockX() && spawner.getLocation().getBlockY() == original.getLocation().getBlockY() && spawner.getLocation().getBlockZ() == original.getLocation().getBlockZ()) {
					originalSpawner = spawner;
					break;
				}
				
			}
		}
		
		int stackAmount = original.getStackAmount() + target.getStackAmount();
		if (stackAmount <= originalSpawner.getSpawnerLimit(false)) {
			return;
		}
		if (stackAmount - originalSpawner.getSpawnerLimit(false) > 10) {
			e.setCancelled(true);
			return;
		}
		target.setStackAmount(stackAmount - originalSpawner.getSpawnerLimit(false), true);
		original.setStackAmount(originalSpawner.getSpawnerLimit(false), true);
		e.setCancelled(true);
		
	}
	
	@EventHandler
	public void onPlace(SpawnerPlaceEvent e) {
		
		NBT nbt = NBT.get(e.getItemInHand());
		int limit = 1;
		int rate = 1;
		int multiplier = 1;
		if (nbt.hasKey("Limit")) {
			limit = nbt.getInt("Limit");
		}
		if (nbt.hasKey("Rate")) {
			rate = nbt.getInt("Rate");
		}
		if (nbt.hasKey("Multiplier")) {
			multiplier = nbt.getInt("Multiplier");
		}

    	Spawner spawnerObj = new Spawner(e.getSpawner().getLocation(), limit, rate, multiplier, UUID.randomUUID().toString());
		if ((limit == 1 && e.getSpawner().getStackAmount() <= 10) || (limit > 1 && e.getSpawner().getStackAmount() <= (25 * (limit - 1)))) {
			if (limit > 1 || rate > 1 || multiplier > 1) {
				dataHandler.getSpawners().add(spawnerObj);
			}
			return;
		}
		for (double x = e.getSpawner().getLocation().getX() - 1; x <= e.getSpawner().getLocation().getX() + 1; x++) {
            for (double y = e.getSpawner().getLocation().getY() - 1; y <= e.getSpawner().getLocation().getY() + 1; y++) {
                for (double z = e.getSpawner().getLocation().getZ() - 1; z <= e.getSpawner().getLocation().getZ() + 1; z++) {
                	Location loc = new Location(e.getSpawner().getLocation().getWorld(), x, y, z);
                	if (loc.getBlockX() == e.getSpawner().getLocation().getBlockX() && loc.getBlockY() == e.getSpawner().getLocation().getBlockY() && loc.getBlockZ() == e.getSpawner().getLocation().getBlockZ()) continue;
                    if (loc.getBlock().getType() == Material.MOB_SPAWNER) {
                    	spawnerObj.setLocation(loc);
                		StackedSpawner target = WildStackerAPI.getStackedSpawner((CreatureSpawner) loc.getBlock().getState());
                    	Spawner targetSpawner = new Spawner(target.getLocation(), 1, 1, 1, UUID.randomUUID().toString());
                		if (dataHandler.getSpawners() != null && !dataHandler.getSpawners().isEmpty()) {
                			for (Spawner spawner : dataHandler.getSpawners()) {
                				if (spawner.getLocation().getBlockX() == targetSpawner.getLocation().getBlockX() && spawner.getLocation().getBlockY() == targetSpawner.getLocation().getBlockY() && spawner.getLocation().getBlockZ() == targetSpawner.getLocation().getBlockZ()) {
                					targetSpawner = spawner;
                					break;
                				}
                				
                			}
                		}
                		
                		int stackAmount = e.getSpawner().getStackAmount() + target.getStackAmount();
                		if (stackAmount > targetSpawner.getSpawnerLimit(false)) {
                			e.setCancelled(true);
                			return;
                		}
                		
                		return;
                    }
                }
            }
        }
		
		e.setCancelled(true);
		
	}

}
