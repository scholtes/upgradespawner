package me.scholtes.upgradespawner.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.bgsoftware.wildstacker.api.events.SpawnerDropEvent;

import me.scholtes.upgradespawner.nbt.NBT;
import me.scholtes.upgradespawner.objects.Spawner;
import me.scholtes.upgradespawner.utils.DataHandler;

public class SpawnerDrop implements Listener {
	
	private DataHandler dataHandler;
	
	public SpawnerDrop(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}
	
	@EventHandler
	public void onDrop(SpawnerDropEvent e) {
		
		int limit = 1;
		int rate = 1;
		int multiplier = 1;
		Location loc = e.getSpawner().getLocation();
		for (Spawner spawner : dataHandler.getSpawners()) {
			Location spawnerLoc = spawner.getLocation();
			if (spawnerLoc.getBlockX() == loc.getBlockX() && spawnerLoc.getBlockY() == loc.getBlockY() && spawnerLoc.getBlockZ() == loc.getBlockZ()) {
				limit = spawner.getLimit();
				rate = spawner.getRate();
				multiplier = spawner.getMultiplier();
				if (e.getSpawner().getStackAmount() == 0) {
					dataHandler.getSpawners().remove(spawner);
				}
				break;
			}
		}
		
		NBT nbt = NBT.get(e.getItemStack());
		nbt.setInt("Limit", limit);
		nbt.setInt("Rate", rate);
		nbt.setInt("Multiplier", multiplier);
		e.setItemStack(nbt.apply(e.getItemStack()));
		
	}
	
}
