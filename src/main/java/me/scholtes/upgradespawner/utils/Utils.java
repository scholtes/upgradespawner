package me.scholtes.upgradespawner.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {

	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}

	public ItemStack guiItem(String name, List<String> desc, Material mat, int amount) {
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.setDisplayName(name);
		iMeta.setLore(desc);
		i.setItemMeta(iMeta);
		return i;
	}

	public ItemStack guiItem(String name, List<String> desc, Material mat, byte data) {
		ItemStack i = new ItemStack(mat, 1, data);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.setDisplayName(name);
		iMeta.setLore(desc);
		i.setItemMeta(iMeta);
		return i;
	}

	public List<String> color(List<String> lore) {
		return lore.stream().map(this::color).collect(Collectors.toList());
	}
}
