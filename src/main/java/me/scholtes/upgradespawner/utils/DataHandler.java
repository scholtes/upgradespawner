package me.scholtes.upgradespawner.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.scholtes.upgradespawner.objects.Spawner;

public class DataHandler {

	private File file = new File("plugins/UpgradeSpawner/", "data.yml");
	private FileConfiguration data = YamlConfiguration.loadConfiguration(file);
	private List<Spawner> spawners = new ArrayList<Spawner>();
	private Map<UUID, Spawner> upgrading = new HashMap<UUID, Spawner>();
	
	public void saveData() {
		data.set("spawners", null);
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (spawners == null || spawners.isEmpty()) {
			return;
		}
		for (Spawner spawner : spawners) {
			double x = spawner.getLocation().getBlockX();
			double y = spawner.getLocation().getBlockY();
			double z = spawner.getLocation().getBlockZ();
			String world = spawner.getLocation().getWorld().getName();
			String spawnerName = spawner.getID();
			data.set("spawners." + spawnerName + ".limit", spawner.getLimit());
			data.set("spawners." + spawnerName + ".rate", spawner.getRate());
			data.set("spawners." + spawnerName + ".multiplier", spawner.getMultiplier());
			data.set("spawners." + spawnerName + ".location.x", x);
			data.set("spawners." + spawnerName + ".location.y", y);
			data.set("spawners." + spawnerName + ".location.z", z);
			data.set("spawners." + spawnerName + ".location.world", world);
		}
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void retrieveData() {
		if (data.getConfigurationSection("spawners") == null) {
			return;
		}
		for (String s : data.getConfigurationSection("spawners").getKeys(false)) {
			double x = data.getDouble("spawners." + s + ".location.x");
			double y = data.getDouble("spawners." + s + ".location.y");
			double z = data.getDouble("spawners." + s + ".location.z");
			World world = Bukkit.getWorld(data.getString("spawners." + s + ".location.world"));
			int limit = data.getInt("spawners." + s + ".limit");
			int rate = data.getInt("spawners." + s + ".rate");
			int multiplier = data.getInt("spawners." + s + ".multiplier");
			Location loc = new Location(world, x, y, z);
			Spawner spawner = new Spawner(loc, limit, rate, multiplier, s);
			spawners.add(spawner);
		}
	}
	
	public List<Spawner> getSpawners() {
		return spawners;
	}

	public Map<UUID, Spawner> getUpgrading() {
		return upgrading;
	}
	
}
