package me.scholtes.upgradespawner.objects;

import org.bukkit.Location;

public class Spawner {
	
	private Location loc;
	private int limit, rate, multiplier;
	String id;
	
	public Spawner(Location loc, int limit, int rate, int multiplier, String id) {
		this.loc = loc;
		this.limit = limit;
		this.rate = rate;
		this.multiplier = multiplier;
		this.id = id;
	}

	public Location getLocation() {
		return loc;
	}

	public void setLocation(Location loc) {
		this.loc = loc;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(int multiplier) {
		this.multiplier = multiplier;
	}
	
	public String getID() {
		return id;
	}
	
	public void setID(String id) {
		this.id = id;
	}
	
	public int getSpawnerLimit(boolean upgrade) {
		int tier = getLimit();
		if (upgrade) { tier += 1; }
		return (tier == 1) ? 10 : 25 * (tier - 1);
	}
	
	public int getSpawnerRate(boolean upgrade) {
		int tier = getRate();
		if (upgrade) tier += 1;
		if (tier == 1) { return 0; }
		if (tier == 2) { return 10; }
		return 25 * (tier - 2);
	}
	
	public int getLimitPrice(boolean remove) {
		int tier = getLimit();
		if (remove) {
			if (tier == 1) { return 0; }
			if (tier == 2) { return 250; }
			if (tier == 3) { return 7500; }
			if (tier == 4) { return 1500; }
			if (tier == 5) { return 2500; }
		}
		return 250 * tier;
	}
	
	public int getRatePrice(boolean remove) {
		int tier = getRate();
		if (remove) {
			if (tier == 1) { return 0; }
			if (tier == 2) { return 250; }
			if (tier == 3) { return 1000; }
			if (tier == 4) { return 2000; }
		}
		if (tier == 1) { return 250; }
		return (tier + 1) * 250;
	}
	
	public int getMultiplierPrice(boolean remove) {
		int tier = getMultiplier();
		if (remove) {
			if (tier == 1) { return 0; }
			if (tier == 2) { return 250; }
			if (tier == 3) { return 1000; }
			if (tier == 4) { return 2000; }
		}
		if (tier == 1) { return 250; }
		return (tier + 1) * 250;
	}
	
}
