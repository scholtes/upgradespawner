package me.scholtes.upgradespawner;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import me.scholtes.upgradespawner.listeners.ClickEvent;
import me.scholtes.upgradespawner.listeners.SpawnEvent;
import me.scholtes.upgradespawner.listeners.SpawnerDrop;
import me.scholtes.upgradespawner.listeners.SpawnerInteract;
import me.scholtes.upgradespawner.listeners.StackListener;
import me.scholtes.upgradespawner.utils.DataHandler;
import me.scholtes.upgradespawner.utils.Utils;

public class UpgradeSpawner extends JavaPlugin {
	
	private DataHandler dataHandler = new DataHandler();
	private Utils utils = new Utils();
	private static UpgradeSpawner instance;
	
	public void onEnable() {

		instance = this;
		getServer().getPluginManager().registerEvents(new StackListener(dataHandler), this);
		getServer().getPluginManager().registerEvents(new SpawnerInteract(dataHandler, utils), this);
		getServer().getPluginManager().registerEvents(new ClickEvent(utils, dataHandler), this);
		//getServer().getPluginManager().registerEvents(new BlockBreak(dataHandler), this);
		getServer().getPluginManager().registerEvents(new SpawnEvent(dataHandler, this), this);
		getServer().getPluginManager().registerEvents(new SpawnerDrop(dataHandler), this);
		
		
		dataHandler.retrieveData();
		new BukkitRunnable() {
			
			@Override
			public void run() {
				dataHandler.saveData();
			}
		}.runTaskTimerAsynchronously(this, 0L, 20 * 60 * 1L);
	}
	
	public void onDisable() {
		dataHandler.saveData();
	}
	


	public static UpgradeSpawner getInstance() {
		return instance;
	}

}
